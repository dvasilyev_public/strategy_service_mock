from flask import Flask, request, jsonify
from werkzeug.exceptions import abort

app = Flask(__name__)


@app.route("/status")
def status():
    return "ok"


@app.route("/api/strategy_service_mock", methods=["POST"])
def mock():
    if not request.json:
        abort(400)
    request.json["a"] = str(request.json["a"])
    for i in range(len(request.json["b"])):
        request.json["b"][i] = str(request.json["b"][i])
    return jsonify(request.json)


if __name__ == "__main__":
    app.run(host="localhost", port=8080)
